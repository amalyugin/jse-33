package ru.t1.malyugin.tm.dto.request.data.load;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.request.AbstractUserRequest;

@NoArgsConstructor
public final class DataXmlJaxBLoadRequest extends AbstractUserRequest {

    public DataXmlJaxBLoadRequest(@Nullable final String token) {
        super(token);
    }

}