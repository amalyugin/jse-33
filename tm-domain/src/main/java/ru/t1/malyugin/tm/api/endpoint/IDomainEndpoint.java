package ru.t1.malyugin.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.dto.request.data.load.*;
import ru.t1.malyugin.tm.dto.request.data.save.*;
import ru.t1.malyugin.tm.dto.response.data.load.*;
import ru.t1.malyugin.tm.dto.response.data.save.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IDomainEndpoint extends IEndpoint {

    @NotNull
    String NAME = "DomainEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, IDomainEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, IDomainEndpoint.class);
    }

    @NotNull
    @WebMethod
    DataBackupLoadResponse loadBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBackupLoadRequest request
    );

    @NotNull
    @WebMethod
    DataBackupSaveResponse saveBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBackupSaveRequest request
    );

    @NotNull
    @WebMethod
    DataBase64LoadResponse loadBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBase64LoadRequest request
    );

    @NotNull
    @WebMethod
    DataBase64SaveResponse saveBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBase64SaveRequest request
    );

    @NotNull
    @WebMethod
    DataBinaryLoadResponse loadBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBinaryLoadRequest request
    );

    @NotNull
    @WebMethod
    DataBinarySaveResponse saveBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBinarySaveRequest request
    );

    @NotNull
    @WebMethod
    DataJsonFasterXmlLoadResponse loadJsonFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonFasterXmlLoadRequest request
    );

    @NotNull
    @WebMethod
    DataJsonFasterXmlSaveResponse saveJsonFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonFasterXmlSaveRequest request
    );

    @NotNull
    @WebMethod
    DataJsonJaxBLoadResponse loadJsonJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonJaxBLoadRequest request
    );

    @NotNull
    @WebMethod
    DataJsonJaxBSaveResponse saveJsonJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonJaxBSaveRequest request
    );

    @NotNull
    @WebMethod
    DataXmlFasterXmlLoadResponse loadXmlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXmlFasterXmlLoadRequest request
    );

    @NotNull
    @WebMethod
    DataXmlFasterXmlSaveResponse saveXmlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXmlFasterXmlSaveRequest request
    );

    @NotNull
    @WebMethod
    DataXmlJaxBLoadResponse loadXmlJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXmlJaxBLoadRequest request
    );

    @NotNull
    @WebMethod
    DataXmlJaxBSaveResponse saveXmlJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXmlJaxBSaveRequest request
    );

    @NotNull
    @WebMethod
    DataYamlFasterXmlLoadResponse loadYaml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataYamlFasterXmlLoadRequest request
    );

    @NotNull
    @WebMethod
    DataYamlFasterXmlSaveResponse saveYaml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataYamlFasterXmlSaveRequest request
    );

}