package ru.t1.malyugin.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.api.service.IServiceLocator;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class Backup {

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final IServiceLocator serviceLocator;

    public Backup(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public void start() {
        load();
        executorService.scheduleWithFixedDelay(this::save, 0, 5, TimeUnit.SECONDS);
    }

    public void stop() {
        executorService.shutdown();
    }

    private void save() {
        serviceLocator.getDomainService().saveDataBackup();
    }

    private void load() {
        if (!serviceLocator.getDomainService().checkBackup()) return;
        serviceLocator.getDomainService().loadDataBackup();
    }

}