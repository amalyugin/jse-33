package ru.t1.malyugin.tm.repository;

import ru.t1.malyugin.tm.api.repository.ISessionRepository;
import ru.t1.malyugin.tm.model.Session;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

}
