package ru.t1.malyugin.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.api.repository.ISessionRepository;
import ru.t1.malyugin.tm.api.service.ISessionService;
import ru.t1.malyugin.tm.model.Session;

public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull final ISessionRepository repository) {
        super(repository);
    }

}