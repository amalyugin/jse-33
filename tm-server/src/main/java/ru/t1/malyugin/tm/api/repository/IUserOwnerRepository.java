package ru.t1.malyugin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnerRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    void clear(@NotNull String userId);

    int getSize(@NotNull String userId);

    @NotNull
    M add(@NotNull String userId, @NotNull M model);

    @Nullable
    M remove(@NotNull String userId, @NotNull M model);

    @Nullable
    M removeById(@NotNull String userId, @NotNull String id);

    @Nullable
    M removeByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    List<M> findAll(@NotNull String userId);

    @NotNull
    List<M> findAll(@NotNull String userId, @NotNull Comparator<M> comparator);

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    M findOneByIndex(@NotNull String userId, @NotNull Integer index);

}